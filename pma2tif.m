%%

filename = "./ha/lowFRET/hel11.pma";
attributes = dir(filename);
fileSize = attributes.bytes;

file = fopen(filename, 'r');
xdim = fread(file, 1, 'int16');
ydim = fread(file, 1, 'int16');
numFrames = (fileSize-4)/(xdim*ydim);
	
% Read the pixels from input file and organized in a 4D tensor (x,y,1 for grayscale, numFrames)
stack = reshape(fread(file, inf, 'uint8=>uint8'), xdim, ydim, 1, numFrames);
fclose(file);
	
% Output a tif stack
outname = [filename(1:end-4) '.tif'];
imwrite(255-2*stack, '~/a.tif');


