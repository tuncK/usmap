%%%


% Ideal parameters
%p = [ 1 0 256 0 1 0 ]; % a b e c d f
%dp = [0 0 0 0 0 0];

% 3 point pairs
p3 = [ 0.9977 -2.5697e-4 254.66 -8.0668e-4 1.0028 -0.14087];
dp3 = [ 0.0163 105.78e-4 2.2998 78.639e-4 0.0058 1.58211];

% 10 point pairs
p10 = [ 0.999 7.2867e-4 254.10 15.644e-4 1.0041 -0.73294 ];
dp10 = [ 0.0017 6.7229e-4 0.28 18.499e-4 0.0006 0.18030 ];


% 20 point pairs
p20 = [ 0.9984 5.6190e-4 254.21 19.264e-4 1.0038 -0.69567];
dp20 = [ 0.0010 3.6901e-4 0.13 8.3494e-4 0.0004 0.07742 ];



[x,y] = meshgrid(1:512,1:512);

if true
	dp = dp20;
	dx = (dp(1)*x).^2 + (dp(2)*y).^2 + dp(3)^2;
	dy = (dp(4)*x).^2 + (dp(5)*y).^2 + dp(6)^2;
	relError = sqrt( dx+dy );
else
	dp = p20;
	p = p10;
	dx = dp(1)*x + dp(2)*y + dp(3) - p(1)*x - p(2)*y - p(3);
	dy = dp(4)*x + dp(5)*y + dp(6) - p(4)*x - p(5)*y - p(6);
	relError = sqrt(dx.^2+dy.^2);
end

relError(:,257:end) = nan;


set(0, 'defaultaxesfontsize', 20);
imagesc(relError);
colorbar
%caxis([0 0.5]);

print('a.png');


