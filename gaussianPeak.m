%%

function [kernel,dx] = gaussianPeak(n,sigma);
	[xx,yy] = meshgrid(-n:1:n,-n:1:n);
	kernel = exp( - (xx.^2+yy.^2) / (2*sigma^2) );
	kernel /= sum(sum(kernel));
	%kernel -= 1/(2*n+1)^2;
	
	dx = -xx .* exp( - (xx.^2+yy.^2) / (2*sigma^2) );
%	dy = -yy .* exp( - (xx.^2+yy.^2) / (2*sigma^2) );
end
