%% Uses point pairs obtained to map the channels by solving
% a system of linear equations.
% Input must be sorted such that they are in pairs.

function mapout = mapping (coordinates, rough, printAplot)
	x = coordinates(:,1);
	y = coordinates(:,2);
	xp = coordinates(:,3);
	yp = coordinates(:,4);
	b = reshape([xp'; yp'],1,[])';
	n = length(x);
	
	if rough
		A = zeros(2*n,6);
		A(1:2:end,:) = [x, y, ones(n,1), zeros(n,3)];
		A(2:2:end,:) = [zeros(n,3), x, y, ones(n,1)];
	else
		A = zeros(2*n,12);
		A(1:2:end,:) = [x, y, x.^2, y.^2, x.*y, ones(n,1), zeros(n,6)];
		A(2:2:end,:) = [zeros(n,6), x, y, x.^2, y.^2, x.*y, ones(n,1)];
	end	
	
	%mapout = inv(A'*A)*A'*b;
	yy = A' * b;
	P = A' * A;
	mapout = P\yy;
end

