% Calculate a mapping based on an initial guess from a previous map.
% Only small perturbations allowed.


more off;
close all;

% Load peak positions from a real file
%filename = "./soni/bead/green.tif";
filename = "./soni/DNAonly/green4.tif";
findpeak;


% Classify peaks according to the channel.
% Boundary region is treated as junk.
segmentationFilename = "./soni/DNAonly/hel2.pma";
segmentationPMA;
x = peakPos(:,1);
y = peakPos(:,2);
r = sqrt(x.^2+y.^2);
theta = atan2(y,x);
delta = r00 - r.*cos(theta00-theta);
threshold = 5;
leftPeaks = peakPos(delta>threshold,:);
rightPeaks = peakPos(delta<-threshold,:);


% TEST: Remove some of the data to check effect of incomplete labelling
%ind1 = 40;
%ind2 = 20;

%rightPeaks = rightPeaks (rand(size(rightPeaks,1),1) > ind1/100, :);
%leftPeaks = leftPeaks (rand(size(leftPeaks,1),1) > ind2/100, :);

% TEST: Add noise to simulate position inaccuracy
%rightPeaks += rand(size(rightPeaks));
%leftPeaks += rand(size(leftPeaks));


% Find angles between neighbours
%[leftAngleList, leftDistList] = findAngles(leftPeaks);
%[rightAngleList, rightDistList] = findAngles(rightPeaks);

%[leftAngleList, leftAngleHist, leftDistList] = findAngleHist(leftPeaks);
%[rightAngleList, rightAngleHist, rightDistList] = findAngleHist(rightPeaks);

leftCounts = findFeatures(leftPeaks);
rightCounts = findFeatures(rightPeaks);


% Use this above lists of angles to find matching points
% Since right is usually less populated, map right to left.
numPeaksRight = size(rightPeaks,1);
numPeaksLeft = size(leftPeaks,1);

angleHitThreshold = 0; % 25 for beads, 40
angleAcceptanceThreshold = 0.005;% 0.005 in radians

distanceHitThreshold = 0;
distanceAcceptanceThreshold = 0.5;% in px 0.5

angleHistThreshold = -1;


countHistThreshold = -10;

maxAngleLength = size(rightAngleList,2);
maxDistLength = size(rightDistList,2);


coordinates = [];
for i=randperm(numPeaksRight)
	idist = rightDistList (i,:);
	idistlength = maxDistLength-sum(isnan(idist));
	
	iangles = rightAngleList (i,:);
	ianglelength = maxAngleLength-sum(isnan(iangles));
	
	% If the distance/angle list shorter than detection threshold, skip.
	if ianglelength < angleHitThreshold || idistlength < distanceHitThreshold  % previously x1.1
		continue;
	end	
	
	matchIdx = nan;
	maxScore = -inf;
	for j=1:numPeaksLeft
		% Check angle hist discrepancy
	%	angleHistScore = size(rightAngleHist,2)-sum( abs(rightAngleHist(i,:) - leftAngleHist(j,:) ));
		
	%	if angleHistScore>angleHistThreshold && angleHistScore > maxScore
	%		maxScore = angleHistScore;
	%		matchIdx = j;
	%	end
		
		score = -sum(abs( rightCounts(i,:)-leftCounts(j,:) ));
		
		if score > countHistThreshold && score > maxScore
			maxScore = score;
			matchIdx = j;
		end
		
		continue; %%%%%%%%%%%%%%%%%%%%%%%%%	
		
		% Check distance matching
		jdist = leftDistList (j,:);
		jdistlength = maxDistLength-sum(isnan(jdist));
		a = 1;
		b = 1;
		distScore = 0;
		while a<=idistlength && b<=jdistlength
			d = idist(a) - jdist(b);
			if d < distanceAcceptanceThreshold
				distScore++;
				a++;
				b++;
			else
				if d<0
					a++;
				else
					b++;
				end
			end
		end
		

		% Check angle matching, only if distance permissive
		if distScore > distanceHitThreshold
			jangles = leftAngleList(j,:);
			janglelength = maxAngleLength-sum(isnan(jangles));
			
			%% Compares list of angles using dynamic programming-like approach
			a = 1;
			b = 1;
			angleScore = 0;
			while a<=ianglelength && b<=janglelength
				d = iangles(a) - jangles(b);
				if abs(d) < angleAcceptanceThreshold
					angleScore++;
					a++;
					b++;
				else
					if d<0
						a++;
					else
						b++;
					end
				end
			end

			totalScore = distScore + angleScore;
			if angleScore>angleHitThreshold && totalScore > maxScore
				maxScore = totalScore;
				matchIdx = j;
			end
		end
	end
	
	if ~isinf(maxScore)
	%	dy = abs(leftPeaks(matchIdx,2)-rightPeaks(i,2));
	%	dx = abs(leftPeaks(matchIdx,1)-rightPeaks(i,1));
	%	if dy > 20 || dx > 275 || dx <220
	%		% Stray hit, skip
	%		continue;
	%	end
	
	maxScore	
	coordinates = [coordinates; leftPeaks(matchIdx,1), leftPeaks(matchIdx,2), rightPeaks(i,1), rightPeaks(i,2)];
		if size(coordinates,1) == 10
			% Enough pairs were already obtained, stop.
			% For performance reasons, roughmap uses only a few point pairs.
			i = inf;
			j = inf;
			break;
		end
	end
end


% Use these detected pairs to calculate the rough mapping
if size(coordinates,1) < 3
	fprintf("Impossible to map, too few constraints\n");
else
	roughmap = mapping(coordinates, true);
end

imshow(1-3*difference);
hold on
plot(leftPeaks(:,1), leftPeaks(:,2), 'go', 'linewidth', 1);
plot(rightPeaks(:,1), rightPeaks(:,2), 'ro', 'linewidth', 1);
if size(coordinates,1) > 0
	line( [coordinates(:,1),coordinates(:,3)]', [coordinates(:,2),coordinates(:,4)]', 'color', 'blue', 'linewidth', 2);
end
hold off;

% Remove the extra whitespace around the figure
fig_pos = get(gcf, 'position');
set(gcf, 'position', [0 0 fig_pos(3) fig_pos(3)]);
set(gca, 'position', [0 0 1 1]);
set(gcf, 'paperpositionmode', 'auto');
%print(sprintf('%d-%d.png', ind1,ind2), '-r300');
%print('~/match.png', '-r300');


% Use the information in rough map to calculate a fine map
if false
	finerMap;
end


