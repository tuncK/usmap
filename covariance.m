%%

usmap;

x = coordinates(:,1);
y = coordinates(:,2);
xp = coordinates(:,3);
yp = coordinates(:,4);
b = reshape([xp'; yp'],1,[])';
	
n = length(x);
A = zeros(2*n,6);
A(1:2:end,:) = [x, y, ones(n,1), zeros(n,3)];
A(2:2:end,:) = [zeros(n,3), x, y, ones(n,1)];
	
K = inv(A'*A);
[xx,yy] = meshgrid((1:512)/512,(1:512)/512);	
uncertaintyx2 = xx.^2 * K(1,1) + yy.^2 * K(2,2) + K(3,3) + 2*xx.*yy*K(1,2) + 2*xx*K(1,3) + 2*yy*K(2,3);
uncertaintyy2 = xx.^2 * K(4,4) + yy.^2 * K(5,5) + K(6,6) + 2*xx.*yy*K(4,5) + 2*xx*K(4,6) + 2*yy*K(5,6);
uncertaintyr = sqrt(uncertaintyx2+uncertaintyy2);
uncertaintyr(:,257:end) = nan;

set(0, 'defaultaxesfontsize', 20);
imagesc(uncertaintyr);
colorbar
print('a.png', '-r300');
close all;
return

% Experimentally observed:
dx = L2Rmapped(1,:) - rightPeaks(:,1);
dy = L2Rmapped(2,:) - rightPeaks(:,2);
distance = sqrt(dx.^2+dy.^2);
[mindist, minindex] = min(distance, [], 1);

outliers = (mindist>1);
imshow(1-2*difference);
hold on
plot(L2Rmapped(1,outliers), L2Rmapped(2,outliers), 'ro', 'linewidth', 1.5);
plot(leftPeaks(outliers,1), leftPeaks(outliers,2), 'go', 'linewidth', 1.5);
hold off;
print('b.png', '-r300');



