%%

for i=42:57
	% Import the movie to be processed
	filename = sprintf("./ha/highFRET/hel%d.pma",i);

	attributes = dir(filename);
	fileSize = attributes.bytes;

	file = fopen(filename, 'r');
	xdim = fread(file, 1, 'int16');
	ydim = fread(file, 1, 'int16');
	numFrames = (fileSize-4)/(xdim*ydim);
		
	% Read the pixels from input file and organized in a 4D tensor (x,y,1 for grayscale, numFrames)
	stack = reshape(fread(file, inf, 'uint8=>uint8'), xdim, ydim, numFrames);
	fclose(file);
	
	%for j=1:20
	%	imshow(255-stack(:,:,j)');
	%	j
	%	pause(0.5);
	%end
	
	%imwrite(uint8(255-mean(stack,3))', sprintf('all%d.tif', i));
	%continue;

	% Extract an image from the movie to process
	red = mean(stack(:,:,1:9),3)'; %1:5 for Woodson's microscope
	green = mean(stack(:,:,11:19),3)'; % 6:15 for Woodson's microscope
	mix = max(red,green);

	imwrite(uint8(255-mix), sprintf('mix%d.tif', i));
%	imwrite(uint8(255-red), 'red4.tif');
%	imwrite(uint8(255-mix), 'mix4.tif');
end


