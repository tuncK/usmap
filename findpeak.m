%%

% Import and normalize the image
input = double(imread(filename));
linput = reshape(input,1,[]);
input = (input-min(linput))/(max(linput)-min(linput));
imsize = size(input,1);


% check if there is a need to invert the file
if sum(linput>100) > sum(linput<100)
	input = 1-input;
end

backgroundKernel = gaussianPeak(15,15);
background = conv2(input,backgroundKernel,'same');
difference = input-background;
difference = difference/max(max(difference));

sorted = reshape(difference,1,[]);
sorted = sort(sorted(rand(length(sorted),1)<0.1), 'descend');
threshold = sorted( round(length(sorted)*0.015) );
% Alternative fixed threshold:
%threshold = 0.2; % 0.05 for beads; 0.2 for Cy3/5
binary = (difference > threshold);


% loop over positions of the peaks
labels = 0*binary;
labelCounter = 1;
imageMargin = 2;
for i=imageMargin:1:imsize-imageMargin
	for j=imageMargin:1:imsize-imageMargin
		if binary(i,j) % Is a peak
			neighbours = unique( [labels(i,j-1), labels(i-1,j-1), labels(i-1,j), labels(i-1,j+1)] );
			neighbours = neighbours(neighbours~=0);
			
			if length(neighbours) == 0 % Isolated point, start a new peak.
				labels(i,j) = labelCounter;
				labelCounter++;
			else
				labels(i,j) = neighbours(1);
				for k=2:length(neighbours)
					% A clash found, correct for redundant indeces.
					labels(labels==neighbours(k)) = neighbours(1);
				end
			end
		end
	end
end


% The labelled image will be processed as a 1D vector instead of a 2D matric
% to improve computational efficiency.
[xx,yy] = meshgrid(1:1:imsize,1:1:imsize);
xx = reshape(xx,1,[]);
yy = reshape(yy,1,[]);
labels = reshape(labels,1,[]);

intensity = reshape(difference,1,[]);
xx = xx .* intensity;
yy = yy .* intensity;


labelList = unique(labels);
labelList(labelList==0) = [];
peakPos = [];
for i=labelList
	peak = find(labels==i);
	peakArea = sum(intensity(peak));
	
	xpos = sum(xx(peak)) / peakArea;
	ypos = sum(yy(peak)) / peakArea;
	peakPos = [peakPos; xpos, ypos];
end

% Discard peaks if too close to the edges
margin = 5;
peakPos(peakPos(:,1) < margin | peakPos(:,1) > imsize-margin | peakPos(:,2) < margin | peakPos(:,2) > imsize-margin, :) = [];


fprintf('%d peaks found\n', size(peakPos,1));
return


imshow(1-2*input);
hold on;
plot(peakPos(:,1),peakPos(:,2), 'co');
hold off

% Remove the extra whitespace around the figure
fig_pos = get(gcf, 'position');
set(gcf, 'position', [fig_pos(1:3) fig_pos(3)]);
set(gca, 'position', [0 0 1 1]);
set(gcf, 'paperpositionmode', 'auto');
print('~/peaks.png')


