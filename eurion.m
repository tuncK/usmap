%% Detects Eurion constellations on the banknotes


input = imread('5lira.jpeg');
grayscale = input(:,:,3);


kdim = 15;
radius = 5;
circleKernel = zeros(kdim,kdim);
[xx,yy] = meshgrid(-floor(kdim/2):floor(kdim/2), -floor(kdim/2):floor(kdim/2));
r = sqrt(xx.^2+yy.^2);
dr = 1.5;
circleKernel( abs(r-radius)<dr ) = 1;
circleKernel( r<=(radius-dr) ) = -1;
circleKernel( r>=(radius+dr) ) = -1;

peaks = conv2(grayscale, circleKernel, 'same');
gkernel = gaussianPeak(10,1);
pos = conv2(-peaks,gkernel,'same');
binary = (pos>7e3);


% loop over positions of the peaks
labels = 0*binary;
labelCounter = 1;
imageMargin = 2;

for i=imageMargin:1:size(labels,1)-imageMargin
	for j=imageMargin:1:size(labels,2)-imageMargin
		if binary(i,j) % Is a peak
			neighbours = unique( [labels(i,j-1), labels(i-1,j-1), labels(i-1,j), labels(i-1,j+1)] );
			neighbours = neighbours(neighbours~=0);
			
			if length(neighbours) == 0 % Isolated point, start a new peak.
				labels(i,j) = labelCounter;
				labelCounter++;
			else
				labels(i,j) = neighbours(1);
				for k=2:length(neighbours)
					% A clash found, correct for redundant indeces.
					labels(labels==neighbours(k)) = neighbours(1);
				end
			end
		end
	end
end


% The labelled image will be processed as a 1D vector instead of a 2D matric
% to improve computational efficiency.
[xx,yy] = meshgrid(1:1:size(labels,2),1:1:size(labels,1));
xx = reshape(xx,1,[]);
yy = reshape(yy,1,[]);
labels2 = labels;
labels = reshape(labels,1,[]);

%labelList = unique(reshape(labels,1,[]));
labelList = unique(labels);
labelList(labelList==0) = [];
peakPos = [];
for i=labelList
	peak = find(labels==i);
	peakArea = length(peak);
	if peakArea > 10 % 20 too big or non-existent peaks
		%continue;
	end
	
	xpos = sum(xx(peak)) / peakArea;
	ypos = sum(yy(peak)) / peakArea;
	peakPos = [peakPos; xpos, ypos];
end


imshow(input);
hold on;
plot(peakPos(:,1),peakPos(:,2), 'r.');
hold off;
print('~/peaks.jpg')


