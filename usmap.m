%% An algorithm to calculate mapping between Cy3/Cy5 channels


more off;
close all;

% Load peak positions from a real file
filename = "./ha/highFRET/mix371.tif";
findpeak;


% Classify peaks according to the channel.
segmentationFilename = "./woodson/DNAonly/hel2.pma";
segmentationPMA;
x = peakPos(:,1);
y = peakPos(:,2);
r = sqrt(x.^2+y.^2);
theta = atan2(y,x);
delta = r00 - r.*cos(theta00-theta);
% Boundary region in-between is treated as junk.
threshold = 5;
leftPeaks = peakPos(delta>threshold,:);
rightPeaks = peakPos(delta<-threshold,:);


leftCounts = findFeatures(leftPeaks);
rightCounts = findFeatures(rightPeaks);

% Use this above lists of angles to find matching points
% Since right is usually less populated, map right to left.
numPeaksRight = size(rightPeaks,1);
numPeaksLeft = size(leftPeaks,1);


countHistThreshold = -8; % -1 for beads, -4 for cy3/5
countStopThreshold = -2; % -2
numPairs = 20; % To auto-detect


tic
coordinates = zeros(numPairs,4);
scores = -inf*ones(numPairs,1);
for i=randperm(numPeaksRight)
	% Find the best match on the left channel that resembles peak i on right.
	iscores = -sum( abs( leftCounts-rightCounts(i,:) ),2);
	[maxScore,matchIdx] = max(iscores);
		
	if sum(maxScore>scores) > 0 % A better score found
		[~,index] = min(scores);
		scores(index) = maxScore;
		coordinates(index,:) = [leftPeaks(matchIdx,1), leftPeaks(matchIdx,2), rightPeaks(i,1), rightPeaks(i,2)];
		
		if sum(scores>=countStopThreshold) == numPairs
			% Enough pairs were already obtained, stop.
			% For performance reasons, roughmap uses only a few point pairs.
			i = inf;
			break;
		end
	end
end
toc


scores

% Check the quality of the pairs found
coordinates = coordinates(scores>=countHistThreshold,:);
scores = scores(scores>=countHistThreshold);


% Optional quality check: the assignments should make parallel lines
% Disregard the ones against the general trend
dx = coordinates(:,1) - coordinates(:,3);
dy = coordinates(:,2) - coordinates(:,4);
outliers = ( (abs(dx-mean(dx))>2*std(dx)) | (abs(dy-mean(dy))>2*std(dy)) );
scores(outliers) = [];
coordinates(outliers,:) = [];
scores'


% Plot the deduced matching
if true
	imshow(1-3*difference);
	hold on
	plot(leftPeaks(:,1), leftPeaks(:,2), 'go', 'linewidth', 1);
	plot(rightPeaks(:,1), rightPeaks(:,2), 'ro', 'linewidth', 1);
	if size(coordinates,1) > 0
		line( [coordinates(:,1),coordinates(:,3)]', [coordinates(:,2),coordinates(:,4)]', 'color', 'blue', 'linewidth', 2);
	end
	hold off;

	% Remove the extra whitespace around the figure
	fig_pos = get(gcf, 'position');
	set(gcf, 'position', [fig_pos(1:3) fig_pos(3)]);
	set(gca, 'position', [0 0 1 1]);
	set(gcf, 'paperpositionmode', 'auto');
	print('~/match.png', '-r300');
end


% Use these detected pairs to calculate the rough mapping
if size(coordinates,1) < 3
	fprintf("Impossible to map, too few constraints\n");
	return
else
	roughmap = mapping(coordinates, true);
	
	% Use the information in rough map to calculate a fine map
	finerMap;
end


