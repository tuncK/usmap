%% Generates a finer map out of a known rough map

L2Rmapped = reshape(roughmap,[],2)' * [ leftPeaks(:,1) leftPeaks(:,2) ones(size(leftPeaks,1),1) ]';

% Find overlapping peaks by choosing closest peak on right side to the mapped peak
% Ignore if even the closest peak is unacceptable far.
dx = L2Rmapped(1,:) - rightPeaks(:,1);
dy = L2Rmapped(2,:) - rightPeaks(:,2);
distance = sqrt(dx.^2+dy.^2);
[mindist, minindex] = min(distance, [], 1);

% Map using all overlapping peaks detected
leftindex = find(mindist<=5);
rightindex = minindex(mindist<=5);
coordinates = [leftPeaks(leftindex,1), leftPeaks(leftindex,2), rightPeaks(rightindex,1), rightPeaks(rightindex,2)];
	

% Will use a random 200 matching pairs, if there are too many.
if size(coordinates,1) > 200
	sel = randperm(size(coordinates,1));
	sel = sel(1:200);
	coordinates = coordinates(sel,:);
end


% All variables scaled to [0,1] for numerical accuracy
finemap = mapping(coordinates/512, false);
finemap([3:5 9:11]) /= 512;
finemap([6 12]) *= 512;
L2Rmapped = reshape(finemap,[],2)' * [ x y x.^2 y.^2 x.*y ones(size(x,1),1) ]';


imshow(1-2*difference);
hold on
plot(L2Rmapped(1,:), L2Rmapped(2,:), 'go', 'linewidth', 2);
plot(rightPeaks(:,1), rightPeaks(:,2), 'r*');
hold off;
print('~/fineMap.png', '-r300');


return

imshow(1-2*difference);
hold on
plot(leftPeaks(:,1), leftPeaks(:,2), 'go', 'linewidth', 1);
plot(rightPeaks(:,1), rightPeaks(:,2), 'ro', 'linewidth', 1);
if size(coordinates,1) > 0
	line( [coordinates(:,1),coordinates(:,3)]', [coordinates(:,2),coordinates(:,4)]', 'color', 'blue', 'linewidth', 2);
end
hold off;
print('~/fineAssociation.png', '-r300');

	

