%% Generates a list of angles that is used as a feature for point matching


function [thetaList, thetaHist, distanceList] = findAngleHist(coordinates)
pkg load statistics;

% Neighbour search parameters
radius1 = 40; %40
radius2 = 100; %50 %70 for bead
maxNumAngles = 100;
maxNumDists = 50;

n = size(coordinates,1);
thetaList = nan*zeros(n,maxNumAngles); % inf vs nan?
distanceList = nan*zeros(n,maxNumDists);

angleBins = 0:0.1:pi;
thetaHist = nan*zeros(n,length(angleBins));

for i=1:n
	x0 = coordinates(i,1);
	y0 = coordinates(i,2);
	
	d = sqrt((coordinates(:,1)-x0).^2 + (coordinates(:,2)-y0).^2);
	d(i) = nan; % self distance, not allowed
	neighboursList = find(d<radius2 & d>radius1);

	pairs = combnk(neighboursList,2);
	id1 = pairs(:,1);
	id2 = pairs(:,2);
	v1x = coordinates(id1,1)-x0;
	v1y = coordinates(id1,2)-y0;	
	v2x = coordinates(id2,1)-x0;
	v2y = coordinates(id2,2)-y0;
		
	tempAngles = sort( acos( (v1x.*v2x+v1y.*v2y) ./ sqrt((v1x.^2+v1y.^2).*(v2x.^2+v2y.^2)) ));
	tempAngles = tempAngles(tempAngles>0.2); %%%%%% ad hoc measure
	tempAngles = tempAngles(1:min(length(tempAngles),maxNumAngles));
	thetaList(i,1:length(tempAngles)) = tempAngles;
	
	tempAngles = sort( acos( (v1x.*v2x+v1y.*v2y) ./ sqrt((v1x.^2+v1y.^2).*(v2x.^2+v2y.^2)) ));
	thetaHist(i,:) = hist(abs(tempAngles(~isnan(tempAngles))), angleBins);
	
	tempDist = sort(d);
	tempDist = tempDist(1:min(length(tempDist),maxNumDists));
	distanceList(i,1:length(tempDist),1) = tempDist;
end


