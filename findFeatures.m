%% Generates a list of angles that is used as a feature for point matching


function countHist = findFeatures(coordinates)
	pkg load statistics;

	% Neighbour search parameters
	radiusBins = 0:2:50; % px;
	angleBins = -pi:0.2:pi;

	dr = radiusBins(2)-radiusBins(1);
	da = angleBins(2)-angleBins(1);

	n = size(coordinates,1);
	countHist = zeros(n,length(radiusBins)*length(angleBins));

	for i=1:n
		x0 = coordinates(i,1);
		y0 = coordinates(i,2);
		
		d = sqrt( (coordinates(:,1)-x0).^2 + (coordinates(:,2)-y0).^2 );
		d(i) = nan; % self distance, not allowed
		
		neighboursList = find( d<radiusBins(end) & d>radiusBins(1) );
		dist = d(neighboursList);
		angle = atan2(coordinates(neighboursList,2)-y0, coordinates(neighboursList,1)-x0);
		
		index = ceil((dist-radiusBins(1))/dr+0.01) + length(radiusBins)*(ceil((angle-angleBins(1)+0.01)/da-1));
		for j=1:length(index)
			countHist(i, index(j) )++;
		end
	end

end


