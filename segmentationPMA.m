%% Splits the FRET images into Cy3 and Cy5 halves by detecting a separation line.


% Import the movie to be processed
segmentationFilename = "./woodson/dnaOnly/hel2.pma";

attributes = dir(segmentationFilename);
fileSize = attributes.bytes;

file = fopen(segmentationFilename, 'r');
xdim = fread(file, 1, 'int16');
ydim = fread(file, 1, 'int16');
numFrames = (fileSize-4)/(xdim*ydim);
	
% Read the pixels from input file and organize in a 4D tensor (x,y,1 for grayscale, numFrames)
stack = reshape(fread(file, inf, 'uint8=>uint8'), xdim, ydim, numFrames);
fclose(file);


% Extract an image from the movie to process
% Use red laser excited frames to find the boundary
% Experiment specific!!!!
red = mean(stack(:,:,[1:5 16:20]),3)';
green = mean(stack(:,:,6:15),3)';
imsize = size(green,1);


processed = (green<14);
n = 25;
kernel = ones(n,n)/n^2;
a = conv2(processed, kernel, 'same');
c = (a>0.4);
[kernel, dkernel] = gaussianPeak(7,3);
dx = conv2(c,dkernel,'same');
dy = conv2(c,dkernel','same');
dgrad = sqrt(dx.^2 + dy.^2);


threshold = 0.9*max(reshape(dgrad,1,[]));
binary = (dgrad>threshold);
mask = zeros(imsize,imsize); % To avoid edge effects
mask(20:492,20:492) = 1;
%mask(100:400,100:400) = 1;
binary = binary .* mask;

[xx,yy] = meshgrid(1:1:imsize,1:1:imsize);
rr = sqrt(xx.^2+yy.^2);
theta = atan2(yy,xx);

[r0,theta0] = meshgrid(0:5:imsize,0:0.05:3*pi);
bins = zeros(size(r0));


[ii,jj] = find(binary);
% If too many points are available, select a subset to improve performance
if length(ii) > 1000
	selected = randperm(length(ii));
	ii = ii(selected);
	jj = jj(selected);
end


for k=1:1:length(ii)
	delta = r0 - rr(ii(k),jj(k)).*cos(theta0-theta(ii(k),jj(k)));
	bins (abs(delta) < 2)++;
end

[ii,jj] = find(bins==max(max(bins)));
ii = ii(1); % If there is a tie, take the first peak
jj = jj(1);
r00 = r0(ii,jj);
theta00 = theta0(ii,jj);


input = green;
out = zeros(imsize,imsize,3);
background = 1-3*input/max(max(input));
out(:,:,1) = background .* (r00-rr.*cos(theta-theta00) < -5);
out(:,:,2) = background .* (r00-rr.*cos(theta-theta00) > 5);
imwrite(out, '~/0degrees.jpg');


imwrite(1-3*input, '~/boundary_inputImage.jpg');
imwrite(uint8(dgrad), '~/boundary_laplacian.jpg');
imwrite(binary, '~/boundary_Q.jpg');
imwrite(uint8(bins),'~/bins.jpg');





