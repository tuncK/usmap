// A program to find mapping relationship between FRET channels.

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <cmath>
#include <tiffio.h> // libtiff-dev package required
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <fstream>


using namespace std;


// Compares list of angles using dynamic programming-like approach
template <typename T>
int dynamicComparison (const vector<T>& x, const vector<T>& y) {
	const static double acceptanceThreshold = 0.005; // in radians
	int a = 0;
	int b = 0;
	int score = 0;
	while (a<x.size() && b<y.size()) {
		T d = x[a] - y[b];
		if ( abs(d) < acceptanceThreshold ) {
			score++;
			a++;
			b++;
		}
		else {
			if (d < 0)
				a++;
			else
				b++;
		}
	}
	
	return score;
}


template <typename T>
vector <vector <int> > findMatchingAngles (vector <vector <T> >& angleList1, vector <vector <T> >& angleList2, const int numMatches2search = 1e3) {
	int size1 = angleList1.size();
	int size2 = angleList2.size();
	
	// Generate a shuffled list of all elements in set 2 to search matches for
	vector <int> randomIndexVector (size2);
	for (int i=1; i<size2; i++)
		randomIndexVector.at(i) = i;
	random_shuffle(randomIndexVector.begin(), randomIndexVector.end());
	
	int hitThreshold = 20;
	vector <vector <int> > listOfHits;
	bool breakFlag = false;
	
	# pragma omp parallel for schedule (dynamic, 10)
	for (int i=0; i<size2; i++) {
		int id = randomIndexVector.at(i);
		int ilength = angleList2.at(id).size();
		if (ilength < hitThreshold) {
			// Cannot find any hits anyways, skip;
			continue;
		}
		
		int bestScore = -1;
		int bestID = -1;
		
		for (int j=0; j<size1; j++) {
			int jlength = angleList1.at(j).size();
			int score = dynamicComparison(angleList1.at(j),angleList2.at(id));
			if (score > bestScore) {
				bestScore = score;
				bestID = j;
			}
		}
		
		if (bestScore > hitThreshold) {
			// Report the match
			vector <int> hit (2);
			hit.at(0) = id;
			hit.at(1) = bestID;
			
			#pragma omp critical
			listOfHits.push_back(hit);
			
			if (listOfHits.size() == numMatches2search) {
				breakFlag = true;
			}
		}
		
		if (breakFlag) {
		 // Enough number of hits already found, stop all branches.
		 	i = size2;
		}
	}
	
	return listOfHits;
}


vector <vector <double> > listAllAngles (const vector <vector <double> >& xypos) {
	int radius1 = 40; // px minimum
	int radius2 = 50; // px maximum
	int maxNumAngles = 50; // Maximum number of angles to report per point
	
	int n = xypos.size();
	vector <vector <double> > thetaList (n);
	
	# pragma omp parallel for schedule (dynamic, 100)
	for (int i=0; i<n; i++) {
		double x0 = xypos[i][0];
		double y0 = xypos[i][1];
		
		vector<vector<double> > vectors2Neighbours;
		for (int j=0; j<n; j++) {
			if (j==i)
				continue;
			
			double x = xypos[j][0];
			double y = xypos[j][1];
			double d = sqrt( (x-x0)*(x-x0)+(y-y0)*(y-y0) );
			
			if(d<radius2 && d>radius1) { // Is a neighbour
				vector<double> temp(2);
				temp.at(0) = (x-x0);
				temp.at(1) = (y-y0);
				vectors2Neighbours.push_back(temp);
			}
		}
		
		// Loop over all neighbourPairs
		int numNeighbours = vectors2Neighbours.size();
		vector <double> angles;
		for (int a=0; a<numNeighbours; a++) {
			for (int b=a+1; b<numNeighbours; b++) {
				double v1x = vectors2Neighbours[a][0];
				double v1y = vectors2Neighbours[a][1];
				double v2x = vectors2Neighbours[b][0];
				double v2y = vectors2Neighbours[b][1];
				double angle = acos( (v1x*v2x+v1y*v2y)/sqrt((v1x*v1x+v1y*v1y)*(v2x*v2x+v2y*v2y)) );
				angles.push_back(angle);
			}
		}
		
		// Record an ascending list of angles, truncate if too short.
		sort(angles.begin(), angles.end());
		if (angles.size() > maxNumAngles) {
			angles.resize(maxNumAngles);
		 	thetaList.at(i) = angles;
		}
	}
	
	return thetaList;
}


vector <vector <double> > findPeakPositions (const vector <vector <bool> >& input) {
	// Loop over the all image pixelwise and assign an integer to each isolated peak
	// If a conflict is discovered, record the conflict and then replace it.
	
	const int imageMargin = 1;
	int imsize = input.size();
	
	vector <vector<int> > labels (imsize);
	for (int i=0; i<imsize; i++)
		labels.at(i).resize(imsize);
	
	map<int,int> conflicts;
	int labelCounter = 1;
	for (int i=imageMargin; i<imsize-imageMargin; i++) {
		for (int j=imageMargin; j<imsize-imageMargin; j++) {
			if (input[i][j] == true) { // is a peak
				int label1 = labels[i-1][j-1];
				int label2 = labels[i-1][j];
				int label3 = labels[i-1][j+1];
				
				// Sort ascendingly
				if (label3 < label1) {
					int temp = label3;
					label3 = label1;
					label1 = temp;
				}
				if (label3 < label2) {
					int temp = label3;
					label3 = label2;
					label2 = temp;
				}
				if (label2 < label1) {
					int temp = label1;
					label1 = label2;
				}
				
				if (label3==0) {
					// Isolated peak
					labels[i][j] = labelCounter;
					labelCounter++;
				}
				else {
					labels[i][j] = label3;
					// Check for and record conflicts
					if (label2!=0 && label3!=label2) {
						conflicts[label2] = label3;
						for (map<int,int>::iterator it=conflicts.begin(); it!=conflicts.end(); ++it) {
							if (it->second == label2)
								conflicts[it->first] = label3;
						}
					}
					
					if (label1!=0 && label2!=label1) {
						conflicts[label1] = label2;
						for (map<int,int>::iterator it=conflicts.begin(); it!=conflicts.end(); ++it) {
							if (it->second == label1)
								conflicts[it->first] = label2;
						}
					}
				}
			}
		}
	}
	
	
	// Resolve conflicts by substitution
	conflicts[0] = 0;
	for (int i=0; i<imsize; i++) {
		for (int j=0; j<imsize; j++) {
			labels[i][j] = conflicts[ labels[i][j] ];
		}
	}
	
	// Find the COM of each peak
	vector<vector<int> > xpos (labelCounter);
	vector<vector<int> > ypos (labelCounter);
	for (int i=0; i<imsize; i++) {
		for (int j=0; j<imsize; j++) {
			int label = labels[i][j];
			xpos.at(label).push_back(i);
			ypos.at(label).push_back(j);
		}
	}
	
	// Calculate the average x and y position of the peak
	int maxAreaCutoff = 20; // skip peak if too big
	vector <vector <double> > poslist; // 0: <x>, 1: <y>
	for (int i=0; i<labelCounter; i++) {
		int area = xpos.at(i).size();
		if (area > 0 && area < maxAreaCutoff) {
			int xsum = 0;
			int ysum = 0;
			for (int j=0; j<area; j++) {
				xsum+=xpos[i][j];
				ysum+=ypos[i][j];
			}
			
			vector<double> temp (2);
			temp[0] = xsum/double(area);
			temp[1] = ysum/double(area);
			poslist.push_back(temp);
		}
	}
	
	
	return poslist;
}


vector <vector <int> > readTif (string filename, int frameNumToRead=1) {
	TIFF *tif=TIFFOpen(filename.c_str(), "r");
	if (tif == NULL) { // Check file accessibility
		cout << "File " << filename << " could not be read." << endl;
		vector <vector <int> > null;
		return null;
	}
	
	// Skip to the desired frame
	TIFFSetDirectory(tif, frameNumToRead);
	
	// Retrieve file characteristics
	uint32 width, height;
	uint16 bytes_per_pixel;
	TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
	TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bytes_per_pixel);
	bytes_per_pixel/=8;
	
	// Read pixel by pixel (and byte by byte if bit depth > 8bit)
	vector <vector <int> > frame (height);
	uint8 *scanline = new uint8[bytes_per_pixel*width];
	for (int i=0; i<height; i++) {
		TIFFReadScanline(tif, scanline, i, 0);
		vector <int> currentRow (width);
		for (int x = 0; x<width; x++) {
			uint32 pixel = 0x00000000;
			for (int k=0; k<bytes_per_pixel; k++)
				*(((uint8 *)(&pixel))+k) = scanline[bytes_per_pixel*x + k];
			currentRow.at(x) = pixel;
		}
		frame.at(i) = currentRow;
	}
		
	// Close the file and return the matrix containing pixel readings
	delete[] scanline;
	TIFFClose(tif);
	return frame;
}


vector<vector<bool> > image2binary (const vector<vector<int> >& input) {
	int imsize = input.size();
	vector<vector<bool> > binary (imsize);
	int threshold = 255*0.3;
	
	for (int i=0; i<imsize; i++) {
		binary.at(i).resize(imsize);
		for (int j=0; j<imsize; j++) {
			if (input[i][j] > threshold)
				binary[i][j] = true;
			else
				binary[i][j] = false;
		}
	}
	
	return binary;
}

/*
vector<vector <int> > segmentImage (const vector<vector <int> >& input) {
	vector <vector <int> > houghHistogram (); // Histogram to count hits in Hough space
	
	int imsize = input.size();
	for (int i=0; i<imsize; i++) {
		for (int j=0; j<imsize; j++) {
			int value = sqrt(dx*dx+dy*dy) / double(1e-6+input[i][j]);
			if (value > threshold) {
				// Is an edge, calculate Hough transform
				
			}
		}
	}
	
	
	vector<vector <int> > labels;
	return labels;
}*/


// Solve a linear system with GSL, LU decomposition
// https://www.gnu.org/software/gsl/doc/html/linalg.html#examples
vector <double> findMapping (const vector <double>& x, const vector <double>& y, const vector <double>& xp, const vector <double>& yp) {
	int n = x.size();
	
	// Equation to solve Ax = b
	// Allocate A and b
	gsl_matrix* b = gsl_matrix_alloc(2*n,1);
	gsl_matrix* A = gsl_matrix_alloc(2*n,6);
	gsl_matrix_set_zero(A);
	for (int i=0; i<2*n; i+=2) {
		gsl_matrix_set(b,i,0,xp[i]);
		gsl_matrix_set(b,i+1,0,yp[i]);
		
		gsl_matrix_set (A, i, 0, x.at(i));
		gsl_matrix_set (A, i, 1, y.at(i));
		gsl_matrix_set (A, i, 2, 1);
		gsl_matrix_set (A, i+1, 3, x.at(i));
		gsl_matrix_set (A, i+1, 4, y.at(i));
		gsl_matrix_set (A, i+1, 5, 1);
	}
	
	// Attack A' A x = A' b
	// B = A' A
	// q = A' b
	// B x = q
	gsl_matrix* B = gsl_matrix_alloc(6,6);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1, A, A, 0, B);
	
	gsl_matrix* q = gsl_matrix_alloc(2*n,1); // matrix?
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1, A, b, 0, q);
	
	gsl_vector* u = gsl_vector_alloc(6);
	gsl_permutation* p = gsl_permutation_alloc (2*n);
	int signum;
	gsl_linalg_LU_decomp (B, p, &signum);
	gsl_linalg_LU_solve (B, p, (gsl_vector*) q, u);
	
	vector <double> out (6);
	for (int i=0; i<6; i++) {
		out.at(i) = gsl_vector_get(u,i);
	}
	
	gsl_permutation_free (p);	
	gsl_matrix_free (b);
	gsl_vector_free (u);
	gsl_matrix_free (A);
	return out;
}


vector<vector <double> > importPos (const string filename) {
	ifstream file (filename.c_str());
		if (file.is_open() == false) {
		cout << "File " << filename << " could not be read." << endl;
		throw(1);
	}

	vector<vector <double> > out(2);

	while (true) {
		double x,y;
		file >> x >> y;
		
		if (file.eof()==true)
			break;
		
	}
	file.close();
	
	out.pop_back();
	return out;
}

// Compile with
// g++ usmap.cpp -fopenmp -ltiff -lgsl -lblas
int main() {
	string filename = "./sampleMovies/hel1.tif";
//	vector <vector <int> > input = readTif (filename);
//	cout << input.size() << endl;
	
//	vector<vector<bool> > binary = image2binary(input);
//	cout << binary.size()*2 << endl;
	
//	vector <vector <double> > peakPos = findPeakPositions (binary);
//	cout << peakPos.size() << endl;
	
	string filename1 = "./sampleMovies/left.peak";
	string filename2 = "./sampleMovies/right.peak";
	vector<vector <double> > peakPos1 = importPos(filename1);
	vector<vector <double> > peakPos2 = importPos(filename2);
	
	vector <vector <double> > angleList1 = listAllAngles (peakPos1);
	vector <vector <double> > angleList2 = listAllAngles (peakPos2);		
	vector <vector <int> > matches = findMatchingAngles (angleList1, angleList2, 20);
	
	int numMatches = matches.size();
	vector<double> x (numMatches);
	vector<double> y (numMatches);
	vector<double> xp (numMatches);
	vector<double> yp (numMatches);
	for (int i=0; i<numMatches; i++) {
		int a = matches[i][0];
		int b = matches[i][1];
		
		x[i] = peakPos1[a][0];
		y[i] = peakPos1[a][1];
		xp[i] = peakPos2[b][0];
		yp[i] = peakPos2[b][1];
	}
	
	vector <double> map = findMapping (x,y,xp,yp);
	for (int i=0; i<6; i++)
		cout << map[i] << "\t";
	cout << endl;

	return 0;
}


